import warnings
import serial
import serial.tools.list_ports
import time
import collections
import matplotlib
import numpy as np

import threading
import simpleaudio as sa
from matplotlib.figure import Figure
song = {
    "evangelion-jazz" : "music/evangelion-jazz.wav",
    "chaos" : "music/chaos.wav",
    "glooms" : "music/glooms.wav",
    "v" : "music/v.wav" 
}


def connect_arduino(baudrate=9600):
    def is_arduino(p):
        return p.manufacturer is not None and 'arduino' in p.manufacturer.lower()

    ports = serial.tools.list_ports.comports()
    arduino_ports = [p for p in ports if is_arduino(p)]

    def port2str(p):
        return "%s - %s (%s)" % (p.device, p.description, p.manufacturer)

    if not arduino_ports:
        portlist = "\n".join([port2str(p) for p in ports])
        raise IOError("No Arduino found\n" + portlist)

    if len(arduino_ports) > 1:
        portlist = "\n".join([port2str(p) for p in ports])
        warnings.warn('Multiple Arduinos found - using the first\n' + portlist)

    selected_port = arduino_ports[0]
    print("Using %s" % port2str(selected_port))
    ser = serial.Serial(selected_port.device, baudrate)
    time.sleep(2)  # this is important it takes time to handshake
    return ser

class MusicPlayer:
    def __init__(self, ser):
        self.ser = ser
        self.song = "evangelion-jazz"
        self.song_object = sa.WaveObject.from_wave_file(song["evangelion-jazz"]) #default song
        self.play_object = self.song_object.play()
 
    def get_data(self):
        try:
            data = self.ser.read_until(b"\n", 255).decode().strip()
            self.song = data 
        except UnicodeDecodeError as e:
            warnings.warn('invalid data got non unicode. This may happen at the start.')
            return

    def play(self):
        self.get_data() #take in new input 
        if self.song not in song:
            return # invalid song
        self.song_object = sa.WaveObject.from_wave_file(song[self.song])
        self.play_object.stop()
        self.play_object = self.song_object.play() #loop
        print("now playing : " + self.song)





# matching sketch at
# https://create.arduino.cc/editor/piti118/d62e2b1e-f304-48b8-99cd-0d57f0375e1c/preview
def main():
    with connect_arduino(9600) as ser:
        music_player = MusicPlayer(ser)
        print("music player ready . . .")
        print("playing default song : evangelion-jazz")
        while True:
            music_player.play()


if __name__ == "__main__":
    main()
