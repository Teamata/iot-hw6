const int FIRST_BTN = 6;
const int SECOND_BTN = 9;
const int THIRD_BTN = 10;
const int FOURTH_BTN = 11;
const int buttons[4] = {FIRST_BTN, SECOND_BTN, THIRD_BTN, FOURTH_BTN};
const String mapping[4] = {"evangelion-jazz", "chaos", "glooms", "v"};
int previous_state[4] = {1, 1, 1, 1};
const long interval = 100;           // interval at which to blink (milliseconds)
long previousMillis = 0;


void setup() {
  // put your setup code here, to run once:
  pinMode(FIRST_BTN, INPUT_PULLUP);
  pinMode(SECOND_BTN, INPUT_PULLUP);
  pinMode(THIRD_BTN, INPUT_PULLUP);
  pinMode(FOURTH_BTN, INPUT_PULLUP);
  Serial.begin(9600);
}

void send() {
  for (int i = 0; i < 4; i++) {
    int buttonState = digitalRead(buttons[i]);
    if (buttonState == LOW && previous_state[i] == HIGH) {
      Serial.println(mapping[i]);
    }
    previous_state[i] = buttonState;
  }
}

void loop() {
  unsigned long currentMillis = millis();
  if(currentMillis - previousMillis > interval){
      previousMillis = currentMillis;  
      send();
   }
}
