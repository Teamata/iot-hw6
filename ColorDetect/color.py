import warnings
import serial
import serial.tools.list_ports
import time
import collections
import matplotlib
import numpy as np
import math 

matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib import style
style.use("ggplot")

# from matplotlib import pyplot as plt
import threading

import tkinter as tk
from matplotlib.figure import Figure

def connect_arduino(baudrate=9600):
    def is_arduino(p):
        return p.manufacturer is not None and 'arduino' in p.manufacturer.lower()

    ports = serial.tools.list_ports.comports()
    arduino_ports = [p for p in ports if is_arduino(p)]

    def port2str(p):
        return "%s - %s (%s)" % (p.device, p.description, p.manufacturer)

    if not arduino_ports:
        portlist = "\n".join([port2str(p) for p in ports])
        raise IOError("No Arduino found\n" + portlist)

    if len(arduino_ports) > 1:
        portlist = "\n".join([port2str(p) for p in ports])
        warnings.warn('Multiple Arduinos found - using the first\n' + portlist)

    selected_port = arduino_ports[0]
    print("Using %s" % port2str(selected_port))
    ser = serial.Serial(selected_port.device, baudrate)
    time.sleep(2)  # this is important it takes time to handshake
    return ser


class DataStream:
    def __init__(self, ser):
        self.ser = ser
        self.red = 0
        self.blue = 0
        self.green = 0
        self.color = ""
        self.lock = threading.Lock()
        self.shouldStop = True
        self.thread = None

    def calculate(self):
        r_dist = math.sqrt((156-self.red)**2 + (60-self.green)**2 + (139-self.blue)**2)
        g_dist = math.sqrt((92-self.red)**2 + (81-self.green)**2 + (155-self.blue)**2)
        b_dist = math.sqrt((81-self.red)**2 + (62-self.green)**2 + (183-self.blue)**2)
        dist_array = [r_dist,g_dist,b_dist]
        min_dist_index = dist_array.index(min(dist_array))
        if min_dist_index == 0:
            self.color = "RED"
        elif min_dist_index == 1:
            self.color = "GREEN"
        elif min_dist_index == 2: 
            self.color = "BLUE"
        else:
            self.color = ""

    def start(self):
        self.thread = threading.Thread(target=self.run, args=())
        self.thread.daemon = True  # Daemonize thread
        self.shouldStop = False
        self.thread.start()

    def run(self):
        while not self.shouldStop:
            self.get_data()

    def stop(self):
        self.shouldStop = True
        if self.thread is not None:
            self.thread.join()
            self.thred = None

    def read_rgb(self):
        self.ser.write(("detect").encode())


    def get_data(self):
        try:
            data = self.ser.read_until(b"\n", 255).decode().strip()
        except UnicodeDecodeError as e:
            warnings.warn('invalid data got non unicode. This may happen at the start.')
            return

        print(data)
        splitted = data.split(' ')

        if len(splitted) == 3:
            r, g, b = splitted
            try:
                int_r, int_g, int_b = float(r), float(g), float(b)
                self.red = int_r
                self.green = int_g
                self.blue = int_b
            except ValueError as e:
                pass

    def get_red(self):
        return self.red 

    def get_blue(self):
        return self.blue

    def get_green(self):
        return self.green

    def get_color(self):
        return self.color 



class LabelValue(tk.Frame):
    def __init__(self, master, vname, vtext, *arg, **kwargs):
        super().__init__(master, *arg, **kwargs)
        self.label = tk.Label(text=vname)
        self.label.pack(side=tk.TOP)
        self.value = tk.Label(textvariable=vtext)
        self.value.pack(side=tk.TOP)


class RGB(tk.Frame):
    def __init__(self, master, streamer, *arg, **kwargs):
        super().__init__(master, *arg, **kwargs)
        self.streamer = streamer
        self.red = tk.IntVar()
        self.blue = tk.IntVar()
        self.green = tk.IntVar()
        self.color = tk.StringVar()
        self.frame = tk.Frame(master)
        self.red_label = LabelValue(self, 'Red', self.red)
        self.red_label.pack()
        self.green_label = LabelValue(self, 'Green', self.green)
        self.green_label.pack()
        self.blue_label = LabelValue(self, 'Blue', self.blue)
        self.blue_label.pack()
        self.detect_button = tk.Button(self.frame, text="Detect",command=self.detect).pack()
        self.frame.pack()
        self.color_label = LabelValue(self, 'Color', self.color)
        self.color_label.pack()

    def detect(self):
        self.streamer.read_rgb()
        self.streamer.get_data()
        self.red.set(self.streamer.get_red())
        self.green.set(self.streamer.get_green())
        self.blue.set(self.streamer.get_blue())
        self.streamer.calculate()
        self.color.set(self.streamer.get_color())


def main():
    with connect_arduino(9600) as ser:
        streamer = DataStream(ser)
        root = tk.Tk()
        rgb = RGB(root, streamer)
        rgb.pack(side=tk.TOP)
        streamer.start()
        root.mainloop()


if __name__ == '__main__':
    main()
