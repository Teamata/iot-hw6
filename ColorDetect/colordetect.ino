const int RED = 2;
const int BLUE = 3;
const int GREEN = 4;
const int RGB[3] = {RED, GREEN, BLUE};
int detected_rgb[3] = {0,0,0};


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(BLUE, OUTPUT);
  pinMode(GREEN, OUTPUT);
  pinMode(RED, OUTPUT);

}

void loop() {
  // put your main code here, to run repeatedly:
//  int sensorValue = analogRead(A0);
//  digitalWrite(RED, HIGH);
//  digitalWrite(BLUE, HIGH);
//  digitalWrite(GREEN, HIGH);
//  Serial.println(sensorValue);
  char buffer[255];
  if (Serial.available()){
    int nbytes = Serial.readBytesUntil('\n', buffer, 254);
    buffer[nbytes] = 0;
    String message = String(buffer);
    if (message = "detect") {
      for (int i = 0; i < 3; i ++) {
          digitalWrite(RGB[i], HIGH);
          delay(1000);
          detected_rgb[i] = analogRead(A0);
          digitalWrite(RGB[i], LOW);
      }
      String sending = String(detected_rgb[0]) + " " + String(detected_rgb[1]) + " " + String(detected_rgb[2]);
      Serial.println(sending);
    }
  }
}
